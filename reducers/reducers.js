import { combineReducers } from 'redux'
import { ADD_TODO, ADD_ANIMAL, ADD_DOMESTIC_ANIMAL, REMOVE_TODO } from '../actions/actions'

/* The reducer is a function that takes two parameters (state and action) to calculate and return updated state */

/* The first function will be used to create new item */
function todo(state, action) {
   console.log('./reducers - todo');
   switch (action.type) {
	
      case ADD_TODO:
         return {
            id: action.id,
            text: action.text,
            type: action.type,
            foo: action.foo
         }

      case ADD_ANIMAL:
         return {
            id: action.id,
            animal: action.animal,
            type: action.type,
            foo: action.foo
         }

      case ADD_DOMESTIC_ANIMAL:
         return {
            id: action.id,
            animal: action.animal,
            type: action.type,
            foo: action.foo
         }
			
      default:
      return state
   }
}

/* the second one will push that item to the list */
function todos(state = [], action) {
   console.log('./reducers - todos');
   switch (action.type) {
	
      case ADD_TODO:
         return [
            ...state,
            todo(undefined, action)
         ]

      case ADD_ANIMAL:
         return [
            ...state,
            todo(undefined, action)
         ]

      case ADD_DOMESTIC_ANIMAL:
         return [
            ...state,
            todo(undefined, action)
         ]

      case REMOVE_TODO:
        console.log('./reducers/reducer.js - REMOVE_TODO');
         var newItems = this.state.items.slice();
         newItems.splice( action.id, 1 );
         this.setState( { items: newItems } );
         return [ 
            ...state 
         ]

      default:
      return state
   }
}

/* At the end we are using combineReducers helper function where we can add any new reducers we might use in the future */
const todoApp = combineReducers({
   todos
})

export default todoApp