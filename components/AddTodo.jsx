import React, { Component, PropTypes } from 'react'

export default class AddTodo extends Component {
   render() {
      return (
         <div>
            <input type = 'text' ref = 'input' />
				
            <button onClick = {(e) => this.handleClickAddTodo(e)}>
               Add
            </button>

            <button onClick = {(e) => this.handleClickAddAnimal(e)}>
               Add Animal
            </button>

            <button onClick = {(e, pet = true) => this.handleClickAddAnimal(e, pet)}>
               Add Animal
            </button>
				
         </div>
      )
   }

   handleClickAddTodo(e) {
      console.log('handleClickAddTodo(e)... - Todo');
      const node = this.refs.input
      const text = node.value.trim()
      this.props.onAddClick(text)
      node.value = ''
   }

   handleClickAddAnimal(e, pet) {
      console.log( 'handleClickAddAnimal(e)... - Animal - pet: ' + pet );
      const node = this.refs.input;
      const text = node.value.trim();
      this.props.onAddAnimalClick(text, pet);
      node.value = '';
   }
}