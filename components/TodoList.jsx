import React, { Component, PropTypes } from 'react'
import Todo from './Todo.jsx'

export default class TodoList extends Component {
   render() {
      console.log('./TodoList.jsx - TodoList');
      return (
         <div>
            Items
            <ul>
                {
                  this.props.todos.map( 
                    todo => {
                      let result = null;

                      if ( todo.type == 'ADD_TODO' ) {
                        result = <Todo key = { todo.id } { ...todo } deleteListedItem = {this.props.deleteListedItem}/>
                      }

                      return result;
                    }
                  ) 
                }
            </ul>
            Animals
            <ul>
                {
                  this.props.todos.map( 
                    todo => {
                      let result = null;

                      if ( todo.type == 'ADD_ANIMAL' ) {
                        result = <Todo key = { todo.id } { ...todo } />
                      }

                      return result;
                    }
                  ) 
                }
            </ul>
            Pets
            <ul>
                {
                  this.props.todos.map( 
                    todo => {
                      let result = null;

                      if ( todo.type == 'ADD_DOMESTIC_ANIMAL' ) {
                        result = <Todo key = { todo.id } { ...todo } />
                      }

                      return result;
                    }
                  ) 
                }
            </ul>
            {console.log(this.props.todos.map(todo => JSON.stringify(todo)))}
         </div>    
      )
   }
}