import React, { Component, PropTypes } from 'react'

export default class Todo extends Component {
   render() {
   	  console.log('./Todo.jsx - Todo');
      return (
         <li>                
            	<div onClick = {this.deleteItem(this.props.id)}>
            		{this.props.id + '.- ' + ( typeof this.props.animal === "undefined" ? this.props.text : this.props.animal ) + ' - ' + this.props.type }
              </div>
         </li>
      )
   }
	
	deleteItem(idItem){
		console.log('./components/Todo.jsx - Todo');
    this.props.deleteListedItem(idItem);
  }
}