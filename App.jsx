import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addTodo, addAnimal, removeTodo } from './actions/actions'

import AddTodo from './components/AddTodo.jsx'
import TodoList from './components/TodoList.jsx'

var ReactCSSTransitionGroup = require('react-addons-css-transition-group');

class App extends Component {
   constructor(props) {
      super(props);

      this.state = {
         items: ['Item 1...', 'Item 2...', 'Item 3...', 'Item 4...']
      }

      //this.handleAdd = this.handleAdd.bind(this);
      //this.handleRemove = this.handleRemove.bind(this);
   }

   /*handleAdd() {
      var newItems = this.state.items.concat([prompt('Create New Item')]);
      this.setState({items: newItems});
   }*/

   /*handleRemove(i) {
      var newItems = this.state.items.slice();
      newItems.splice(i, 1);
      this.setState({items: newItems});
   }*/

   componentWillMount() {
      console.log('Component WILL MOUNT!')
   }

   componentDidMount() {
      console.log('Component DID MOUNT!')
   }

   componentWillReceiveProps(newProps) {    
      console.log('Component WILL RECIEVE PROPS!')
   }

   shouldComponentUpdate(newProps, newState) { // by default this method returns TRUE
      return true;
   }

   componentWillUpdate(nextProps, nextState) {
      console.log('Component WILL UPDATE!');
   }

   componentDidUpdate(prevProps, prevState) {
      console.log('Component DID UPDATE!')
   }

   componentWillUnmount() {
      console.log('Component WILL UNMOUNT!')
   }

   render() {
      const { dispatch, visibleTodos } = this.props;
  
      var items = this.state.items.map(function(item, i) {
         return (
            <div key = {item} onClick = { (i) => dispatch( removeTodo( i ) ) }>
               {item}
            </div>
         );
      
      }.bind(this));

      return (
         <div>
            <button onClick = {this.handleAdd}>Add Item By Prompt</button>
            <AddTodo onAddClick = { text => dispatch( addTodo( text ) ) } onAddAnimalClick = {(text, pet) => dispatch( addAnimal( text, pet ) ) } />
        
            <ReactCSSTransitionGroup transitionName = "example"
               transitionAppear = { true } transitionAppearTimeout = { 1500 }
               transitionEnter = { false } transitionLeave = {false}>
              <TodoList todos = { visibleTodos } deleteListedItem = {(i) => dispatch( removeTodo(i) ) }/>
            </ReactCSSTransitionGroup>

            <ReactCSSTransitionGroup transitionName = "example"
               transitionAppear = { true } transitionAppearTimeout = { 400 }
               transitionEnter = { false } transitionLeave = { false }>
               
               <h1>My Element...</h1>
               {items}
            </ReactCSSTransitionGroup>
      
         </div>
      )
   }
}

/* Connects to store */ 
function select(state) {
   return {
      visibleTodos: state.todos
   }
}

export default connect(select)(App)