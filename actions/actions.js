export const ADD_TODO = 'ADD_TODO'
export const ADD_ANIMAL = 'ADD_ANIMAL'
export const ADD_DOMESTIC_ANIMAL = 'ADD_DOMESTIC_ANIMAL'

let nextTodoId = 0;

export function addTodo(text) {
	console.log('./actions - addTodo');
   return {
      type: ADD_TODO,
      id: nextTodoId++,
      foo: 'item',
      text
   };
}

export function addAnimal(animal, pet) {
	console.log('./actions - addAnimal');
	return {
	  type: pet ? ADD_DOMESTIC_ANIMAL : ADD_ANIMAL,
	  id: nextTodoId++,
	  foo: 'animal',
	  animal
	};
}

export function removeTodo(todoId) {
	console.log('./actions - removeTodo');
  
  return {
  	id: todoId
  }
}